import numpy as np
import matplotlib.pyplot as plt


int2bin = {
    0:'0000',
    1:'0001',
    2:'0010',
    3:'0011',
    4:'0100',
    5:'0101',
    6:'0110',
    7:'0111',
    8:'1000',
    9:'1001',
    10:'1010'    
}

bin2int = {
    '0000':0,
    '0001':1,
    '0010':2,
    '0011':3,
    '0100':4,
    '0101':5,
    '0110':6,
    '0111':7,
    '1000':8,
    '1001':9,
    '1010':10
}

valids = [
    '0000', '0001', '0010', '0011', 
    '0100', '0101', '0110', '0111', 
    '1000', '1001', '1010'
]

gains = {
    0:[0, 0, 0, 0],
    1:[0.28, 0.25, 0.15, 0.20],
    2:[0.45, 0.41, 0.25, 0.33],
    3:[0.65, 0.55, 0.4, 0.42],
    4:[0.78, 0.65, 0.5, 0.48],
    5:[0.9, 0.75, 0.62, 0.53],
    6:[1.02, 0.8, 0.73, 0.56],
    7:[1.13, 0.85, 0.82, 0.58],
    8:[1.23, 0.88, 0.9, 0.6],
    9:[1.32, 0.9, 0.96, 0.6],
    10:[1.38, 0.9, 1.0, 0.6]
}


class chromosome():
    '''
        Clase cromosoma donde los atributos son:
            1. valor del cromosoma (lista)
            2. valor de la aptitud
    '''
    def __init__(self, ch):
        self.ch = ch
        self.fitness = 0

def selection(population):
    '''
        Seleccion por torneo deterministico
    '''
    winners = []
    fights = np.random.randint(0, len(population), (len(population), 2))
    
    for f in fights:
        if population[f[0]].fitness > population[f[1]].fitness:
            winners.append(population[f[0]])
        else:
            winners.append(population[f[1]])
    
    return winners

def chrom2string(ch):
    '''
        Convertir a cadena de caracteres (longitud 4),
        el cromosoma proporcionado
    '''
    strCh = []
    for i, j in zip(range(0, 13, 4), range(4, 17, 4)):
        strCh.append(''.join([str(gen) for gen in ch[i:j]]))
    
    return strCh

def validate(ch):
    '''
        Verifica si el cromosoma es valido
    '''
    strCh = chrom2string(ch)
    validCh = []
    
    for i in range(4):
        if strCh[i] in valids:
            validCh.append(True)
        else: 
            validCh.append(False)
    
    return validCh


def crossover(ch1, ch2):
    '''
        Cruza en 2 puntos de los cromosomas.
        Genera dos hijos y regresa el mejor.
        Se realiza una validacion de los cromosomas, 
        si es invalida, se repite el proceso de generar
        los puntos de cruza aleatorios.
    '''
    is_valid = False
    
    # Validacion de los cromosomas hijos
    while is_valid == False:
        
        r = np.random.randint(1, 15, (2))
        while r[0] == r[1]:
            r = np.random.randint(1, 15, (2))
        mini = np.min(r)
        maxi = np.max(r)
        #print('Punto de cruza 1 = ', mini)
        #print('Punto de cruza 2 = ', maxi)
        son1 = chromosome([0]*16)
        son2 = chromosome([0]*16)
    
        son1.ch[:mini] = ch1.ch[:mini]
        son1.ch[mini:maxi] = ch2.ch[mini:maxi]
        son1.ch[maxi:] = ch1.ch[maxi:]
    
        son2.ch[:mini] = ch2.ch[:mini]
        son2.ch[mini:maxi] = ch1.ch[mini:maxi]
        son2.ch[maxi:] = ch2.ch[maxi:]
    
        if False in validate(son1.ch) or False in validate(son2.ch):
            is_valid = False
        else:
            is_valid = True
    
    # Seleccion del mejor hijo
    son1.fitness = fitness(son1.ch)
    son2.fitness = fitness(son2.ch)
    
    if son1.fitness > son2.fitness:
        return son1
    else:
        return son2

def cross(population, cross_rate):
    '''
        Cruza de la poblacion proporcionada
    '''
    r = np.random.rand(len(population))
    new_population = [0]*len(population)
    who_cross_pairs = []
    crossed = []
    
    # Obtener los indices de los cromosomas cuya probabilidad
    # de cruza es menor que la tasa de cruza indicada
    who_cross = np.where(r < cross_rate)[0]
    who_dont = np.where(r > cross_rate)[0]
    
    # Determinar las parejar de cromosomas que se cruzaran
    for i in range(len(who_cross)-1):
        who_cross_pairs.append([who_cross[i], who_cross[i+1]])
    who_cross_pairs.append([who_cross[i], who_cross[-1]])

    # Realizar la cruza de las parejas de cromosomas
    for i in range(len(who_cross_pairs)):
        ch1 = population[who_cross_pairs[i][0]]
        ch2 = population[who_cross_pairs[i][1]]
        crossed.append(crossover(ch1, ch2))
    
    # Crear la nueva poblacion, llenando primero los lugares de 
    # los cromosomas que se cruzaron, y despues los que no se cruzaron    
    for i, j in zip(who_cross, range(len(who_cross))):
        new_population[i] = crossed[j]
    for i in who_dont:
        new_population[i] = population[i]
    
    return [new_population, len(who_cross)]
        

def mutation(ch):
    '''
        Mutacion del cromosoma por intercambio reciproco.
        El cromosoma mutado es validado, en caso de no 
        serlo, se genera otra mutacion valida.
    '''
    is_valid = False
    
    while is_valid == False:
    
        new_ch = ch 
        indx = np.random.randint(0, 16)
        move_to = np.random.randint(0, 16)
        #print('Indice 1 = ', indx)
        #print('Indice 2 = ', move_to)
        while move_to == indx:
            move_to = np.random.randint(0, 16)
        a = ch.ch[move_to]        
        new_ch.ch[move_to] = ch.ch[indx]
        new_ch.ch[indx] = a
        
        if False in validate(new_ch.ch):
            is_valid = False
        else:
            is_valid = True  
    
    return new_ch


def mutate(population, m_rate):
    '''
        Mutacion de la poblacion
    '''
    mutated = []
    new_population = [0]*50
    
    r = np.random.rand(50)
    who_mutates = np.where(r < m_rate)[0]
    who_dont = np.where(r > m_rate)[0]
    
    for i in who_mutates:
        mutated.append(mutation(population[i]))
    
    # Creacion de la nueva poblacion
    for i, j in zip(who_mutates, range(len(who_mutates))):
        new_population[i] = mutated[j]
    for i in who_dont:
        new_population[i] = population[i]
        
    return [new_population, len(who_mutates)]
    

def fitness(ch):
    '''
        Calcular la aptitud del cromosoma
    '''
    invertions = []                              
    gain = []
    
    strCh = chrom2string(ch)
    
    for i in range(4):
        invertions.append(bin2int[strCh[i]])
    
    for i, j in zip(range(4), range(4)):
        gain.append(gains[invertions[i]][j])
    
    V = np.abs(np.sum(invertions)-10)
    
    return np.sum(gain)/(500*V+1)


def gpopulation(psize):
    '''
        Generar la poblacion de forma aleatoria
    '''

    population = []
    ch = []

    for k in range(psize):
        integers = np.random.randint(0, 11, (4,))
        binaries = [int2bin[num] for num in integers]
        for b in binaries:
            for i in range(4):
                ch.append(int(b[i]))
        chrom = chromosome(ch)
        population.append(chrom)
        ch = []

    return population

def main():

    psize = 50              # tama;o de la poblacion
    gsize = 20              # numero de generaciones
    cross_rate = 0.8        # probabilidad de cruza
    mutation_rate = 0.01    # probabilidad de mutacion
    pmeans = []             # aptitud media de cada generacion
    fitMean = 0
    
    file = open('log.txt', 'w')
    
    population = gpopulation(psize)
    
    print('\nPoblacion inicial:')
    for p in population:
        print(p.ch, end=' f= ')
        print(fitness(p.ch))
        
    # Crear 20 generaciones de individuos
    for k in range(gsize):
        for p in population:
            p.fitness = fitness(p.ch)
            fitMean += p.fitness
        pmeans.append(fitMean/50)
        fitMean = 0
        
            
        population = selection(population)
        population, ncros = cross(population, cross_rate)
        population, nmuts = mutate(population, mutation_rate)
        
        
        file.write('\n*** Generacion ' + str(k+1) + '\n\n')
        file.write('Cruzas = ' + str(ncros))
        file.write('\nMutaciones = ' + str(nmuts) + '\n\n')
        for p in population:
            file.write(str(p.ch) + ' fitness = ' + str(p.fitness) + '\n')
        
    file.close()
        
    print('\nPoblacion final:')
    for p in population:
        print(p.ch, end=' f=')
        print(p.fitness)

    # Seleccionar el mejor individuo de la ultima generacion
    solution = sorted(population, key=lambda population: population.fitness, reverse=True)[0]
    
    print('\n\nSolucion encontrada')
    print('\nCromosoma:')
    print(solution.ch)
    print(chrom2string(solution.ch))
    solution_intCh = [bin2int[gen] for gen in chrom2string(solution.ch)]
    print(solution_intCh)
    print('\nAptitud = ', solution.fitness)
    print('Inversion total = ', np.sum(solution_intCh))
    print('Ganancias:')
    print('Tienda 1 = ', gains[solution_intCh[0]][0])
    print('Tienda 2 = ', gains[solution_intCh[1]][1])
    print('Tienda 3 = ', gains[solution_intCh[2]][2])
    print('Tienda 4 = ', gains[solution_intCh[3]][3])
    
    plt.plot(np.arange(20), pmeans, 'r')
    plt.title('Aptitud promedio x generación')
    plt.xticks(np.arange(21))
    plt.xlabel('generación')
    plt.ylabel('aptitud')
    plt.legend(['tasa de cruza = ' + str(int(cross_rate*100)) + '%' + '\ntasa de mutacion = ' + str(int(mutation_rate*100)) + '%'])
    plt.show()


if __name__ == "__main__":
    main()