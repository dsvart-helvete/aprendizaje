import keras 
import tweepy 
import numpy as np
import matplotlib.pyplot as plt
from textblob import TextBlob


def auth_twitterAPI(consumer_key, consumer_secret, access_token, access_token_secret):
    '''
        Autenticacion a la API de twitter usando las credendiales
        proporcionadas. Regresa un objeto de tipo API para poder
        interactuar.
    '''
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    return tweepy.API(auth)

def map_sigmoid(x):
    return 1/(1+np.exp(-x))

def main():

    # Credenciales de acceso a la API de Twitter
    consumer_key = 'vfQb9kxCIjsh22W6ELzHkhXR1'
    consumer_secret = 'kb9ZrvMkOt7PyHF6z2ForwlGVlBQfErvqJX6LedxiSqCJqLiOn'
    access_token = '999147172078665728-gthwoMJqMFpMq5O6wSic8ADU6ZAvkR1'
    access_token_secret = 'GcksDLxSRwiD99KBHwOS0cUmmjvvhOSWADQvUmN8IRnsx'

    api = auth_twitterAPI(consumer_key, consumer_secret, access_token, access_token_secret)

    # Cargar el modelo de la RNN LSTM previamente entrenado
    rnn = keras.models.load_model('model/model.h5')
    rnn.load_weights('model/weights.h5')

    # Buscar al menos n tweets con el tema de interes proporcionado
    topic = input('Write the topic of interest: ')
    num_tweets = int(input('Write the number of tweets: '))
    tweets = tweepy.Cursor(api.search, q=topic, lang='en').items(num_tweets)
    tweets_text = [tweet.text for tweet in tweets]

    # Realizar tokenizacion de los tweets, con un vocabulatio de 10000 palabras
    # Tambien se realiza padding o truncating de los tweets para que 
    # las dimensiones del vector de entrada sean las mismas
    tokenizer = keras.preprocessing.text.Tokenizer(num_words = 10000)
    tokenizer.fit_on_texts(tweets_text)
    tweets_tokens = tokenizer.texts_to_sequences(tweets_text)
    tweets_padding = keras.preprocessing.sequence.pad_sequences(tweets_tokens, maxlen=25, padding='pre', truncating='pre')

    '''
        Prediccion usando red neuronal recurrente entrenada
    '''
    # Predecir la clase de los tweets 
    prediction_rnn = rnn.predict(tweets_padding)

    # Formar una lista con tuplas (tweet, score)
    sentiments_rnn = [(tweet, score) for tweet, score in zip(tweets_text, prediction_rnn)]

    # Calcular la polaridad de los tweets usando como refencia un umbral
    thresh = 0.50
    tweets_score_rnn = np.array([0 if score<=thresh else 1 for _, score in sentiments_rnn])
    positive_tweets_rnn = np.count_nonzero(tweets_score_rnn==1)
    negative_tweets_rnn = np.count_nonzero(tweets_score_rnn==0) 

    '''
        Prediccion usando el modulo TextBlob
    '''
    prediction_tb = []
    sentiments_tb = []

    for tweet in tweets_text:
        prediction = TextBlob(tweet).sentiment.polarity
        prediction_tb.append(prediction)    
        sentiments_tb.append((tweet, prediction))

    #prediction_tb = map_sigmoid(np.array(prediction_tb))
    prediction_tb = np.array(prediction_tb)

    # Calcular la polaridad de los tweets usando como refencia un umbral
    thresh = 0.0
    tweets_score_tb = np.array([0 if score<=thresh else 1 for _, score in sentiments_tb])
    positive_tweets_tb = np.count_nonzero(tweets_score_tb==1)
    negative_tweets_tb = np.count_nonzero(tweets_score_tb==0)


    with open('log_rnn.txt', 'w') as log:
        for tweet, score in sentiments_rnn:
            log.write('\nText:') 
            log.write('\n' + tweet)
            log.write('\nscore: %s' % score)
            log.write('\n\n\n')

    with open('log_textblob.txt', 'w') as log:
        for tweet, score in sentiments_tb:
            log.write('\nText:') 
            log.write('\n' + tweet)
            log.write('\nscore: %s' % score)
            log.write('\n\n\n')


    labels_rnn = ['Positive: '+str(positive_tweets_rnn/num_tweets*100)+'%', 'Negative: '+str(negative_tweets_rnn/num_tweets*100)+'%']
    labels_tb = ['Positive: '+str(positive_tweets_tb/num_tweets*100)+'%', 'Negative: '+str(negative_tweets_tb/num_tweets*100)+'%']


    plt.figure().canvas.set_window_title('Comparacion de predicciones')
    plt.subplot(121)
    patches_rnn, _ = plt.pie([positive_tweets_rnn, negative_tweets_rnn], colors=['blue', 'red'])
    plt.legend(patches_rnn, labels=labels_rnn, title='Polarity', loc='best')
    plt.title('Red neuronal recurrente') 
    plt.subplot(122)
    patches_tb, _ = plt.pie([positive_tweets_tb, negative_tweets_tb], colors=['blue', 'red'])
    plt.legend(patches_tb, labels=labels_tb, title='Polarity', loc='best')
    plt.title('Prediccion Textblob')
    plt.show()  

if __name__ == "__main__":
    main()