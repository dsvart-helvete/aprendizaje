Para poder ejecutar el programa, se necesitan los siguientes modulos:

1. tensorflow: 
pip install tensorflow

2. keras:
pip install keras

3. dask:
pip install dask

4. pandas:
pip install pandas

5. tweepy:
pip install tweepy

6. matplotlib:
pip install matplotlib

7. numpy:
pip install numpy



Para leer los datos en formato '.csv':
pip install "dask[complete]" 