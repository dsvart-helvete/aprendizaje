import re
import numpy as np
import tkinter as tk
import pandas as pd
from tkinter import messagebox
from sklearn.neighbors import KNeighborsClassifier


# Archivo de caracteres
df_patterns = pd.read_csv('patterns.csv', sep='\s*,\s*', header=0, engine='python')

# Archivo de targets
df_target = pd.read_csv('targets.csv', sep='\s*,\s*', header=0, engine='python')

# Preparacion de los datos de entrenamiento, eliminando 
# las columnas de los dataframes que no son necesarias 
X = df_patterns.drop(columns='z')
y = df_target.drop(columns='c') 

# Creacion del clasificador KNN
clf = KNeighborsClassifier(n_neighbors=1)

# Entrenamiento del clasificador
clf.fit(X, y)

# Diccionario que indica la correspondencia 
# entre el vector target y su caracter
characterDict = {
                 '000000':'0', '000001':'1', '000010':'2', '000011':'3', 
                 '000100':'4', '000101':'5', '000110':'6', '000111':'7',
                 '001000':'8', '001001':'9', '100000':'A', '100001':'B',
                 '100010':'C', '100011':'D', '100100':'E', '100101':'F',
                 '100110':'G', '100111':'H', '101000':'I', '101001':'J',
                 '101010':'K', '101011':'L', '101100':'M', '101101':'N',
                 '101110':'O', '101111':'P', '110000':'Q', '110001':'R',
                 '110010':'S', '110011':'T', '110100':'U', '110101':'V', 
                 '110110':'W', '110111':'X', '111000':'Y', '111001':'Z'
                }

# Creando la interfaz grafica
root = tk.Tk()
root.resizable(width=False, height=False)
root.geometry('600x500+10+10')

upFrame = tk.Frame(root)
upFrame.pack(side=tk.TOP)

downFrame = tk.Frame(root)
downFrame.pack(side=tk.BOTTOM)

canvas = tk.Canvas(upFrame, width=600, height=450, bg='gray')
canvas.pack()

canvas.create_oval(100, 10, 285, 30, fill='black')
canvas.create_oval(315, 10, 500, 30, fill='black')
canvas.create_oval(500, 20, 520, 205, fill='black')
canvas.create_oval(500, 225, 520, 410, fill='black')
canvas.create_oval(315, 410, 500, 430, fill='black')
canvas.create_oval(100, 410, 285, 430, fill='black')
canvas.create_oval(80, 225, 100, 410, fill='black')
canvas.create_oval(80, 20, 100, 205, fill='black')
canvas.create_polygon(100, 30, 120, 30, 280, 200, 260, 200, fill='black')
canvas.create_oval(290, 20, 310, 205, fill='black')
canvas.create_polygon(320, 200, 340, 200, 500, 30, 480, 30, fill='black')
canvas.create_oval(100, 210, 285, 230, fill='black')
canvas.create_oval(315, 210, 500, 230, fill='black')
canvas.create_polygon(100, 410, 120, 410, 280, 240, 260, 240, fill='black')
canvas.create_oval(290, 225, 310, 415, fill='black')
canvas.create_polygon(320, 240, 340, 240, 500, 410, 480, 410, fill='black')

# Lista donde se almacena el patron de entrada (leido desde la caja de texto)
pattern = []

def update_display():
    global pattern
    colors = {'0':'black', '1':'red'}
    a = canvas.create_oval(100, 10, 285, 30, fill=colors[str(pattern[0])])
    b = canvas.create_oval(315, 10, 500, 30, fill=colors[str(pattern[1])])    
    c = canvas.create_oval(500, 20, 520, 205, fill=colors[str(pattern[2])]) 
    d = canvas.create_oval(500, 225, 520, 410, fill=colors[str(pattern[3])]) 
    e = canvas.create_oval(315, 410, 500, 430, fill=colors[str(pattern[4])]) 
    f = canvas.create_oval(100, 410, 285, 430, fill=colors[str(pattern[5])])
    g = canvas.create_oval(80, 225, 100, 410, fill=colors[str(pattern[6])])
    h = canvas.create_oval(80, 20, 100, 205, fill=colors[str(pattern[7])])
    i = canvas.create_polygon(100, 30, 120, 30, 280, 200, 260, 200, fill=colors[str(pattern[8])])
    j = canvas.create_oval(290, 20, 310, 205, fill=colors[str(pattern[9])])
    k = canvas.create_polygon(320, 200, 340, 200, 500, 30, 480, 30, fill=colors[str(pattern[10])]) 
    l = canvas.create_oval(100, 210, 285, 230, fill=colors[str(pattern[11])])
    m = canvas.create_oval(315, 210, 500, 230, fill=colors[str(pattern[12])])
    n = canvas.create_polygon(100, 410, 120, 410, 280, 240, 260, 240, fill=colors[str(pattern[13])])
    o = canvas.create_oval(290, 225, 310, 415, fill=colors[str(pattern[14])])
    p = canvas.create_polygon(320, 240, 340, 240, 500, 410, 480, 410, fill=colors[str(pattern[15])])

# Leer el texto de entrada desde el widget entry, cuando se presiona el boton
def click_read():
    global pattern
    num_correct = 0
    text = entry.get().split(',')
    for num in text:
        if re.match('^[0|1]$', str(num)):
            num_correct += 1
        else:
            messagebox.showerror('Error', 'El patron de entrada no es valido!')
            break
    if len(text) < 16 or len(text) > 16:
        messagebox.showerror('Error', 'El patron de entrada no es valido!')
    elif num_correct == 16:
        pattern = np.array([int(num) for num in text])
        update_display()


# Predecir el caracter a partir del patron introducido en  la gui, al presionar el boton
def click_predict():
    global pattern
    if len(pattern) != 0:
        Y_test = clf.predict(pattern.reshape(1, -1))
        Y_test = [str(num) for num in Y_test[0]]
        try:
            Y_test_char = characterDict[''.join(Y_test)]
        except KeyError:
            messagebox.showerror('Error', 'El caracter no es valido!')
        messagebox.showinfo('Caracter', 'El caracter reconocido es ' + Y_test_char)
    else:
        messagebox.showerror('Error', 'El patron no tiene informacion!')

labelInfo = tk.Label(downFrame, text='Introduce el patron')
labelInfo.grid(row=0, column=0)

entry = tk.Entry(downFrame, width=25)
entry.grid(row=0, column=1)

button_read = tk.Button(downFrame, text='Leer', width=20, command=click_read)
button_read.grid(row=1, column=0)

button_predict = tk.Button(downFrame, text='Predecir', width=20, command=click_predict)
button_predict.grid(row=1, column=1)

root.mainloop()